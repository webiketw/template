<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/13
 * Time: 下午 05:05
 */

namespace App;
/**
 * Class PDOFactory
 * @package App
 */
class PDOFactory
{
    /**
     * @var mixed|null
     */
    protected $connections;

    /**
     * PDOFactory constructor.
     * @param null $connections
     */
    public function __construct($connections = null)
    {
        $this->connections = $connections ? $connections : include __DIR__ . "/../config/database.php";
    }

    public function getAvailableConnections()
    {
        return array_keys($this->connections);
    }

    /**
     * @param $conn_name
     * @return \PDO
     */
    public function getConnection($db_name)
    {
        $conn_info=$this->connections[$db_name];

        $servername = $conn_info['host'];
        $port = $conn_info['port'];
        $database =$conn_info['database'];
        $username = $conn_info['username'];
        $password = $conn_info['password'];

        try {
            $conn = new \PDO("mysql:host=$servername;dbname=$database;port=$port", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
//            echo "Connected successfully";
        } catch (\PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }

        return $conn;
    }


}