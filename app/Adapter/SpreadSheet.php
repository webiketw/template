<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/15
 * Time: 下午 05:06
 */

namespace App\Adapter;

use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;

class SpreadSheet extends AbstractAdapter
{
    protected $service;
    protected $spreadsheetId;
    protected $range;
    protected $requestBody;
    protected $params;

    public function __construct()
    {
        $client = $this->getClient();
        $this->service = new Google_Service_Sheets($client);
        $this->params = [
            'valueInputOption' => 'RAW'
        ];
    }

    public function setUrl($url)
    {
        $re = '/[-\w]{25,}/m';
        preg_match_all($re, $url, $matches, PREG_SET_ORDER, 0);
        //acually set spreadsheet id
        $this->spreadsheetId = $matches[0][0];
    }

    public function setRange($range)
    {
        $this->range = $range;
    }

    public function setData($array_data)
    {
        $this->requestBody = new Google_Service_Sheets_ValueRange(array(
            'values' => $array_data
        ));
    }

    /**
     * @param $option
     */
    //RAW || USER_ENTERED
    public function setValueInputOption($option)
    {
        $this->params = [
            'valueInputOption' => $option
        ];
    }

    public function send()
    {
        $response = $this->service->spreadsheets_values->update($this->spreadsheetId, $this->range, $this->requestBody, $this->params);
        return $response;
    }
}