<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/15
 * Time: 下午 04:57
 */

namespace App\Adapter;

use Google_Client;
use Google_Service_Sheets;

abstract class AbstractAdapter
{

    function getClient()
    {
        $client = new Google_Client;
        $client->setApplicationName('EG PHP Client');
        $client->setAuthConfig(__DIR__ . '\..\..\credentials.json');
        $client->addScope(Google_Service_Sheets::DRIVE);
        $client->setAccessType('offline');

        return $client;

    }

}