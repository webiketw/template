<?php
namespace Project;
require __DIR__ . '/../vendor/autoload.php';

use App\PDOFactory;
use App\Adapter\SpreadSheet;

$PDOFactory = new PDOFactory();
$conn = $PDOFactory->getConnection('warehouse');
$sql = "SELECT
	* 
FROM
	`core_brands` 
ORDER BY
	created_at DESC 
	LIMIT 100";
$brands = $conn->query($sql)->fetchAll(\PDO::FETCH_ASSOC);

$values = [
    ['品牌', '敘述']
];
foreach ($brands as $brand) {
    $values[] = [
        $brand['title'],
        $brand['description'],
    ];
}


$spreadsheet = new SpreadSheet();
$spreadsheet->setUrl('https://docs.google.com/spreadsheets/d/1VPwMn9DPawOLJ6Nd2YO6kQb6ZlgjcdGWZhXhxIq5pOs/edit#gid=1461016217');
$spreadsheet->setRange('TEST!A1');
$spreadsheet->setData($values);
$response = $spreadsheet->send();

